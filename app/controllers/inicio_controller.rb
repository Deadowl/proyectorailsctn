class InicioController < ApplicationController
  def index
    if params[:tipo] 
      @elementos = Elemento.where(tipo_id: params[:tipo]) 
    else
      @elementos = Elemento.all
    end
    @tipos = Tipo.all
  end
end
