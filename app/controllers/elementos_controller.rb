class ElementosController < ApplicationController
  before_action :set_elemento, only: [:show, :edit, :update, :destroy]

  def index
    @elementos = Elemento.all
  end

  def show
    @comentarios = Comentario.where(elemento_id: @elemento) 
  end

  def new
    @elemento = Elemento.new
  end

  def edit
  end

  def create
    @elemento = Elemento.new(elemento_params)

    respond_to do |format|
      if @elemento.save
        format.html { redirect_to @elemento, notice: 'Elemento creado exitosamente' }
        format.json { render :show, status: :created, location: @elemento }
      else
        format.html { render :new }
        format.json { render json: @elemento.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @elemento.update(elemento_params)
        format.html { redirect_to @elemento, notice: 'Elemento actualizado exitosamente.' }
        format.json { render :show, status: :ok, location: @elemento }
      else
        format.html { render :edit }
        format.json { render json: @elemento.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @elemento.destroy
    respond_to do |format|
      format.html { redirect_to elementos_url, notice: 'Elemento borrado exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    def set_elemento
      @elemento = Elemento.find(params[:id])
    end

    def elemento_params
      params.require(:elemento).permit(:nombre, :descripcion, :imagen, :foto, :precio, :caracteristicas, :tipo_id, :created_at, :updated_at)
    end
end
