class CreateEjemplos < ActiveRecord::Migration
  def change
    create_table :ejemplos do |t|
      t.string :nombre
      t.integer :numero

      t.timestamps
    end
  end
end
