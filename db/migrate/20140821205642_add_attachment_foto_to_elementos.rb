class AddAttachmentFotoToElementos < ActiveRecord::Migration
  def self.up
    change_table :elementos do |t|
      t.attachment :foto
    end
  end

  def self.down
    remove_attachment :elementos, :foto
  end
end
